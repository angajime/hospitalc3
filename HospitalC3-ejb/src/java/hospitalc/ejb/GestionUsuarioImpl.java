/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitalc.ejb;

import hospitalc.entidades.ATS;
import hospitalc.entidades.ActividadCalificada;
import hospitalc.entidades.Evento;
import hospitalc.entidades.Medico;
import hospitalc.entidades.Mensaje;
import hospitalc.entidades.P_Especial;
import hospitalc.entidades.Paciente;
import hospitalc.entidades.PersonalSanitario;
import hospitalc.entidades.SucesoLaboral;
import hospitalc.entidades.Usuario;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Adrian Marin
 */
@Stateless
public class GestionUsuarioImpl implements GestionUsuario {

    @PersistenceContext(unitName = "HospitalC3-ejbPU")
    private EntityManager em;

    @Override
    public Error registarUsuario(Usuario u) {
        Usuario us = em.find(Usuario.class, u.getDNI());
        if (us == null) {
            em.persist(u);
            return Error.NO_ERROR;
        } else {
            us.setContrasenia(u.getContrasenia());
            us.setCiudad(u.getCiudad());
            us.setCorreoE(u.getCorreoE());
            us.setMovil(u.getMovil());
            us.setDireccion(u.getDireccion());
            us.setTelefono(u.getTelefono());
            return Error.NO_ERROR;
        }
    }

    @Override
    public Error compruebaLogin(Usuario u) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if(u.getDNI()==null)
            return Error.CUENTA_INEXISTENTE;
        Usuario user = em.find(Usuario.class, u.getDNI());
        if (user == null) {
            return Error.CUENTA_INEXISTENTE;
        }
        if (user.getContrasenia().equals(u.getContrasenia())) {
            return Error.NO_ERROR;
        } else {
            return Error.CONTRASENIA_INCORRECTA;
        }

    }

    @Override
    public Error modificarDatosPersonales(Usuario u) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Error e = compruebaLogin(u);
        if (e != Error.NO_ERROR) {
            return e;
        }
        em.merge(u);
        return Error.NO_ERROR;
    }

    @Override
    public List<Evento> listaEventos(Usuario u) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        List<Evento> eventos = null;
        if (compruebaLogin(u) == Error.NO_ERROR) {
            for (Evento e : u.getEventos()) {
                eventos.add(e);
            }
        }
        return eventos;
    }

    @Override
    public List<Mensaje> listaMensajes(Usuario u) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        List<Mensaje> mensajes = null;
        if (compruebaLogin(u) == Error.NO_ERROR) {
            for (Mensaje m : u.getMensajes()) {
                mensajes.add(m);
            }
        }
        return mensajes;

    }

    @Override
    public List<PersonalSanitario> listaPersonalSanitario(Paciente u) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        List<PersonalSanitario> personal = null;
        if (compruebaLogin(u) == Error.NO_ERROR) {
            personal.add(u.getMedicoCabecera());
            for (ATS ats : u.getAts()) {
                personal.add(ats);
            }
        }
        return personal;
    }

    @Override
    public List<Paciente> listaPacientes(PersonalSanitario ps) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        List<Paciente> pacientes = null;
        if (compruebaLogin(ps) == Error.NO_ERROR) {
            if (ps instanceof ATS) {
                for (Paciente a : ((ATS) ps).getPacientes()) {
                    pacientes.add(a);
                }
            } else if (ps instanceof Medico) {
                for (Paciente b : ((Medico) ps).getPacientes()) {
                    pacientes.add(b);
                }
            }
        }
        return pacientes;
    }

    @Override
    public List<ActividadCalificada> listaActividades(Medico m) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        List<ActividadCalificada> actividades = null;
        if (compruebaLogin(m) == Error.NO_ERROR) {
            for (ActividadCalificada act : m.getActividadesCalificadas()) {
                actividades.add(act);
            }
        }
        return actividades;
    }

    @Override
    public List<SucesoLaboral> listaSucesos(PersonalSanitario ps) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        List<SucesoLaboral> sucesos = null;
        if (compruebaLogin(ps) == Error.NO_ERROR) {
            for (SucesoLaboral suc : ps.getSucesosLaborales1()) {
                sucesos.add(suc);
            }
        }
        return sucesos;
    }

    @Override
    public List<P_Especial> listaPacEsp(Medico m) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        List<P_Especial> pacEspeciales = null;
        if (compruebaLogin(m) == Error.NO_ERROR) {
            for (Paciente p : m.getPacientes()) {
                if (p instanceof P_Especial) {
                    pacEspeciales.add((P_Especial) p);
                }
            }
        }
        return pacEspeciales;
    }

    @Override
    public Error addEvento(Usuario u, Evento e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (compruebaLogin(u) == Error.NO_ERROR) {
            Collection<Evento> ev = u.getEventos();
            ev.add(e);
            u.setEventos(ev);
            em.merge(u);
            return Error.NO_ERROR;
        } else {
            return Error.CUENTA_INEXISTENTE;
        }
    }

    @Override
    public Error addMensaje(Usuario u, Mensaje m) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (compruebaLogin(u) == Error.NO_ERROR) {
            Collection<Mensaje> ev = u.getMensajes();
            ev.add(m);
            u.setMensajes(ev);
            em.merge(u);
            em.merge(m);
            return Error.NO_ERROR;
        } else {
            return Error.CUENTA_INEXISTENTE;
        }
    }

    @Override
    public Error addPersonalSanitario(PersonalSanitario ps) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        PersonalSanitario ps1 = em.find(PersonalSanitario.class, ps.getDNI());
        if (ps1 == null) {
            em.persist(ps);
            return Error.NO_ERROR;
        } else {
            return Error.CUENTA_YA_EXISTENTE;
        }
    }

    @Override
    public Error addPaciente(Paciente p, PersonalSanitario ps) {
        if(aniadirPaciente(p, ps) == Error.CUENTA_YA_EXISTENTE)
            return Error.CUENTA_YA_EXISTENTE;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Collection<ATS> listaATS = p.getAts();
        Medico m = p.getMedicoCabecera();
        if (!listaATS.contains(ps) && m != null) {
            if (ps instanceof Medico) {
                p.setMedicoCabecera((Medico) ps);
                Collection<Paciente> pacientes = ((Medico)ps).getPacientes();
                pacientes.add(p);
                ((Medico)ps).setPacientes(pacientes);
                em.merge(p);
                em.merge(ps);
            } else {
                listaATS.add((ATS) ps);
                p.setAts(listaATS);
                Collection<Paciente> pacientesATS = ((ATS)ps).getPacientes();
                pacientesATS.add(p);
                ((ATS)ps).setPacientes(pacientesATS);
                em.merge(p);
                em.merge(ps);
            }
            
            return Error.NO_ERROR;
        }
        //En el caso de que sea nulo deberia llegar aqui.
        return Error.CUENTA_INEXISTENTE;
    }
    public Error aniadirPaciente(Paciente p, PersonalSanitario ps) {
        Paciente p1 = em.find(Paciente.class, p.getDNI());
        if(p1==null) {
            em.persist(p);
            this.addPaciente(p, ps);
            return Error.NO_ERROR;
        }else {
            return Error.CUENTA_INEXISTENTE;
        }
    }

    @Override
    public Error addActividad(Medico m, ActividadCalificada a) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            Collection<ActividadCalificada> acts = m.getActividadesCalificadas();
            if (!acts.contains(a)) {
                acts.add(a);
                m.setActividadesCalificadas(acts);
                em.merge(m);
                em.merge(a);
                return Error.NO_ERROR;
            }   
        return Error.CUENTA_INEXISTENTE;
    }

    @Override
    public Error addSuceso(PersonalSanitario ps, SucesoLaboral sl) {
        PersonalSanitario ps1 = em.find(PersonalSanitario.class, ps.getDNI());
        if (ps1 == null) {
            Collection<SucesoLaboral> sucesos = ps.getSucesosLaborales1();
            if(!sucesos.contains(sl)) {
                sucesos.add(sl);
                ps.setSucesosLaborales1(sucesos);
                em.merge(ps);
                em.merge(sl);
                return Error.NO_ERROR;
            }
        }
        return Error.CUENTA_INEXISTENTE;
    }

    @Override
    public Usuario getUsuarioByDNI(String DNI) {
        Usuario u = em.find(Usuario.class, DNI);
        return u;
    }

}
