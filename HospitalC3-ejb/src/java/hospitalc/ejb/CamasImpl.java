/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitalc.ejb;

import hospitalc.entidades.Cama;
import hospitalc.entidades.Paciente;
import hospitalc.entidades.Planta;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Antonio Gutiérrez Ojeda
 */
@Stateless
public class CamasImpl implements Camas {

    @PersistenceContext(unitName = "HospitalC3-ejbPU")
    private EntityManager em;
    
    //devuelve la localizacion del paciente
    @Override
    public Cama encontrarPaciente(Paciente p) {
    throw new UnsupportedOperationException("Not supported yet.");    
    //return p.getCama();
    }

    
    //Asigna una cama a un usuario concreto cambiando su disponibilidad
    @Override
    public Error asignarCama(Cama c) {
        Cama cam = em.find(Cama.class, c.getN_Cama());
        if (cam == null) {
            return Error.CAMA_INEXISTENTE;
        }else if (cam.getLibre()==false){
            return Error.CAMA_OCUPADA;
        }
        else {
            cam.setLibre(false);
            cam.setPaciente(c.getPaciente());
            em.merge(cam);
            return Error.NO_ERROR;
        }
    }

    //Libera una cama dejandola disponible para nuevos pacientes
    @Override
    public Error liberarCama(Cama c) {
        Cama cam = em.find(Cama.class, c.getN_Cama());
        if (cam == null) {
            return Error.CAMA_INEXISTENTE;
        } else {
            cam.setLibre(true);
            em.merge(cam);
            return Error.NO_ERROR;
        }

    }

    //Devuelve una lista con todas las camas disponibles en una planta
    @Override
    public List<Cama> verDisponibles(Planta p) {
        Planta floor = em.find(Planta.class, p.getIdPlanta());
        List<Cama> lista = new ArrayList<Cama>();
        for (Cama cam : floor.getCamas()) {
            if (cam.getLibre()) {
                lista.add(cam);
            }
        }
        return lista;
    }

    //Da error si la cama no esta disponible, comprueba disponibilidad
    @Override
    public Error estaDisponible(Cama c) {
        Cama cam = em.find(Cama.class, c.getN_Cama());
        if (cam == null) {
            return Error.CAMA_INEXISTENTE;
        } else if (cam.getLibre()) {
            return Error.NO_ERROR;
        } else {
            return Error.CAMA_OCUPADA;
        }
    }

}
