/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospitalc.ejb;

import hospitalc.entidades.ActividadCalificada;
import hospitalc.entidades.Evento;
import hospitalc.entidades.Medico;
import hospitalc.entidades.Mensaje;
import hospitalc.entidades.P_Especial;
import hospitalc.entidades.Paciente;
import hospitalc.entidades.PersonalSanitario;
import hospitalc.entidades.SucesoLaboral;
import hospitalc.entidades.Usuario;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Adrian Marin
 */
@Local
public interface GestionUsuario {
    public static enum Error {
        NO_ERROR,
        CUENTA_INEXISTENTE,
        CONTRASENIA_INCORRECTA,
        CUENTA_YA_EXISTENTE
    }
    public Error registarUsuario(Usuario u);
    public Error compruebaLogin(Usuario u);
    public Error modificarDatosPersonales(Usuario u);
    public List<Evento> listaEventos(Usuario u);
    public List<Mensaje> listaMensajes(Usuario u);
    public List<PersonalSanitario> listaPersonalSanitario (Paciente u);
    public List<Paciente> listaPacientes (PersonalSanitario ps);
    public List<ActividadCalificada> listaActividades (Medico m);
    public List<SucesoLaboral> listaSucesos(PersonalSanitario ps);
    public List<P_Especial> listaPacEsp(Medico m);
    public Error addEvento(Usuario u, Evento e);
    public Error addMensaje(Usuario u, Mensaje m);
    public Error addPersonalSanitario(PersonalSanitario ps);
    public Error addPaciente(Paciente p, PersonalSanitario ps);
    public Error addActividad(Medico m, ActividadCalificada a);
    public Error addSuceso(PersonalSanitario ps, SucesoLaboral sl);
    public Usuario getUsuarioByDNI(String DNI);
}
