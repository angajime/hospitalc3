/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitalc.ejb;

import hospitalc.entidades.Almacen;
import hospitalc.entidades.MaterialSanitario;
import hospitalc.entidades.Planta;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class GestionAlmacenImpl implements GestionAlmacen {

    @PersistenceContext(unitName = "HospitalC3-ejbPU")
    private EntityManager em;

    private Almacen storage;

    //Se añade material a la base de datos, si ya existe, se actualiza la cantidad de ese material 
    @Override
    public void agnadirMaterial(MaterialSanitario ms) {
        MaterialSanitario mat = em.find(MaterialSanitario.class, ms.getId());
        if (mat == null) {
            Almacen a = em.find(Almacen.class, ms.getAlmacen().getIdAlmacen());
            if (a == null) {
                a = new Almacen();
                a.setIdAlmacen(ms.getAlmacen().getIdAlmacen());
                a.setDescripcion("General");
                a.setMateriales(new LinkedList<MaterialSanitario>());
                em.persist(a);
            }
            a.getMateriales().add(ms);
            ms.setAlmacen(a);
            em.persist(ms);
            em.merge(a);
        } else if (mat.getNombre().equals(ms.getNombre())) {  //Se comprueba que tengan el mismo nombre.
            mat.setCantidad(mat.getCantidad() + ms.getCantidad());
            em.merge(mat);
        } else {
            //ERROR
        }

    }

    //Cambia el almacén donde se guarda el material indicado
    @Override
    public void cambiarAlmacen(MaterialSanitario ms) {
        if (getMaterial(ms) != null) {
            MaterialSanitario mat = em.find(MaterialSanitario.class, ms);
            mat.setAlmacen(ms.getAlmacen());
            em.merge(mat);
        }
    }

    //Quita el numero de materiales solicitados o en su defecto, el máximo
    @Override
    public void quitarMaterial(MaterialSanitario ms) {
        MaterialSanitario mat = em.find(MaterialSanitario.class, ms.getId());
        if (mat.getCantidad() <= ms.getCantidad()) {
            System.out.println("La cantidad sacada es: " + mat.getCantidad());
            mat.setCantidad(0);
            em.merge(mat);
        } else {
            System.out.println("La cantidad sacada es: " + ms.getCantidad());
            mat.setCantidad(mat.getCantidad() - ms.getCantidad());
            em.merge(mat);
        }
    }

    //Se consulta el material solicitado
    @Override
    public MaterialSanitario getMaterial(MaterialSanitario ms) {
        MaterialSanitario mat = em.find(MaterialSanitario.class, ms);
        if (mat == null) {
            System.err.println("El material solcitado no está disponible");
            return null;
        } else {
            return mat;
        }
    }

    //TO-DO: Borrar y rehacer por Adolfo
    @Override
    public List<MaterialSanitario> getInventario(Almacen almacen) {
        Almacen a = em.find(Almacen.class, almacen.getIdAlmacen());
        List<MaterialSanitario> lm = new LinkedList<>();
        if(a != null)
            return a.getMateriales();
        else
            return lm;
    }

}
