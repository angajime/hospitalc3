/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitalc.ejb;

import hospitalc.entidades.Cama;
import hospitalc.entidades.Paciente;
import hospitalc.entidades.Planta;
import java.util.List;
import javax.ejb.Local;


/**
 *
 * @author alumno
 */
@Local
public interface Camas {
    public static enum Error{
        NO_ERROR,
        CAMA_OCUPADA,
        SIN_CAMAS,
        CAMA_INEXISTENTE
    };
    
    public Cama encontrarPaciente(Paciente p);
    public Error asignarCama(Cama c);
    public Error liberarCama(Cama c);
    public List<Cama> verDisponibles(Planta p);
    public Error estaDisponible(Cama p);
    
    
}
