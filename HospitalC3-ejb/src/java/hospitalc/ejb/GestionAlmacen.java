/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitalc.ejb;
import hospitalc.entidades.Almacen;
import hospitalc.entidades.MaterialSanitario;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author alumno
 */

@Local
public interface GestionAlmacen {
    
    public void agnadirMaterial(MaterialSanitario ms);
    public void cambiarAlmacen (MaterialSanitario ms);
    public void quitarMaterial(MaterialSanitario ms);
    public MaterialSanitario getMaterial(MaterialSanitario ms);
    public List<MaterialSanitario> getInventario(Almacen almacen);
    
}
