package hospitalc.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Almacen implements Serializable {

    @ManyToOne(optional = false, targetEntity = Planta.class)
    private Planta planta;

    @OneToMany(targetEntity = MaterialSanitario.class, mappedBy = "almacen")
    private List<MaterialSanitario> materiales;
    
    @Basic
    private String Descripcion;

    @Id
    private Long idAlmacen;

    public Almacen() {

    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Almacen) && (this.getIdAlmacen().equals(((Almacen) o).getIdAlmacen())); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return (int) (31 * getIdAlmacen() +1);
    }

    @Override
    public String toString() {
        return idAlmacen.toString() + " " + planta.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    public Planta getPlanta() {
        return this.planta;
    }

    public void setPlanta(Planta planta) {
        this.planta = planta;
    }

    public List<MaterialSanitario> getMateriales() {
        return materiales;
    }
    
    public String getDescripcion() {
        return this.Descripcion;
    }

    public void setMateriales(List<MaterialSanitario> materiales) {
        this.materiales = materiales;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public Long getIdAlmacen() {
        return this.idAlmacen;
    }

    public void setIdAlmacen(Long idAlmacen) {
        this.idAlmacen = idAlmacen;
    }

}
