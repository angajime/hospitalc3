package hospitalc.entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class PersonalSanitario extends Usuario implements Serializable {

    @OneToMany(targetEntity = SucesoLaboral.class)
    private Collection<SucesoLaboral> sucesosLaborales1;

    @ManyToOne(optional = false, targetEntity = Hospital.class)
    private Hospital hospital;

    @Basic
    private String ID_Trabajador;

    public PersonalSanitario() {

    }

    public void setSucesosLaborales1(Collection<SucesoLaboral> sucesosLaborales1) {
        this.sucesosLaborales1 = sucesosLaborales1;
    }

    public Collection<SucesoLaboral> getSucesosLaborales1() {
        return sucesosLaborales1;
    }

    public String getID_Trabajador() {
        return this.ID_Trabajador;
    }

    public void setID_Trabajador(String ID_Trabajador) {
        this.ID_Trabajador = ID_Trabajador;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

}
