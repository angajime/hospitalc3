package hospitalc.entidades;

import java.io.Serializable;

import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Column;
import javax.persistence.JoinColumn;

@Entity
public class Planta implements Serializable {

    @Id
    private Long idPlanta;

    @Basic
    private String Piso;

    @OneToMany(targetEntity = Almacen.class, mappedBy = "planta")
    @JoinColumn(nullable = true)
    private Collection<Almacen> almacenes;

    @OneToMany(targetEntity = Cama.class, mappedBy = "planta")
    @JoinColumn(nullable = true)
    private Collection<Cama> camas;

    @ManyToOne(optional = false, targetEntity = Hospital.class)
    private Hospital hospital;

    public Planta() {

    }

    @Override
    public int hashCode() {
        return (int) (31 * this.getIdPlanta() + 1);
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Planta) && (this.idPlanta.equals(((Planta) o).idPlanta)); //To change body of generated methods, choose Tools | Templates.
    }

    public Long getIdPlanta() {
        return this.idPlanta;
    }

    @Override
    public String toString() {
        return idPlanta.toString() + " " + Piso.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    public void setIdPlanta(Long idPlanta) {
        this.idPlanta = idPlanta;
    }

    public String getPiso() {
        return this.Piso;
    }

    public void setPiso(String Piso) {
        this.Piso = Piso;
    }

    public Collection<Almacen> getAlmacenes() {
        return this.almacenes;
    }

    public void setAlmacenes(Collection<Almacen> almacenes) {
        this.almacenes = almacenes;
    }

    public Collection<Cama> getCamas() {
        return this.camas;
    }

    public void setCamas(Collection<Cama> camas) {
        this.camas = camas;
    }

    public Hospital getHospital() {
        return this.hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

}
