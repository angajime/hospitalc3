package hospitalc.entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class HistorialClinico implements Serializable {

    @OneToOne(optional = false, targetEntity = Paciente.class, mappedBy = "historialClinico1")
    private Paciente paciente1;

    @Id
    private String id;

    @OneToMany(targetEntity = Episodio.class)
    private Collection<Episodio> episodios;

    @OneToMany(targetEntity = Contradiccion.class)
    @JoinColumn(nullable = true)
    private Collection<Contradiccion> contradicciones;

    public HistorialClinico() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
       return 31 * Integer.parseInt(this.getId()) +1;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof HistorialClinico) && (this.id.equals(((HistorialClinico) o).id)); //To change body of generated methods, choose Tools | Templates.
    }

    public Paciente getPaciente1() {
        return this.paciente1;
    }

    public void setPaciente1(Paciente paciente1) {
        this.paciente1 = paciente1;
    }

    public Collection<Episodio> getEpisodios() {
        return this.episodios;
    }

    public void setEpisodios(Collection<Episodio> episodios) {
        this.episodios = episodios;
    }

    public Collection<Contradiccion> getContradicciones() {
        return this.contradicciones;
    }

    public void setContradicciones(Collection<Contradiccion> contradicciones) {
        this.contradicciones = contradicciones;
    }

    @Override
    public String toString() {
        return id.toString() + " " + paciente1.toString(); //To change body of generated methods, choose Tools | Templates.
    }

}
