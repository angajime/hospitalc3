/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospitalc.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Adrian Marin
 */

@Entity
public class MaterialSanitario implements Serializable{
    @ManyToOne(optional = false, targetEntity = Almacen.class)
    private Almacen almacen;
    
    @Id
    private Integer id;
    
    @Basic
    private String nombre;
    
    @Basic 
    private String tipo;
    
    @Basic
    private Integer cantidad;

    @Override
    public boolean equals(Object o) {
        return (o instanceof MaterialSanitario) && (this.id.equals(((MaterialSanitario)o).id));
    }

    @Override
    public int hashCode() {
        //Debo preguntar al profesor
        return (31 * this.getId() +1);
    }
    
    public Integer getCantidad() {
        return cantidad;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }
    
    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
        
    
}
