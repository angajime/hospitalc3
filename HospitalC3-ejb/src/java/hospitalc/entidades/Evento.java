package hospitalc.entidades;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Evento implements Serializable {

    @ManyToOne(optional = false, targetEntity = Usuario.class)
    private Usuario usuario;
    @Id
    private Long id;

    @Basic
    private String descripcion;

    @Basic
    private Timestamp fechayhora;

    public Evento() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return (int) (31 * this.getId() + 1);
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Evento) && (this.id.equals(((Evento) o).id)); //To change body of generated methods, choose Tools | Templates.
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechayhora() {
        return fechayhora;
    }

    public void setFechayhora(Timestamp fechayhora) {
        this.fechayhora = fechayhora;
    }

    @Override
    public String toString() {
        return id.toString() + fechayhora.toString(); //To change body of generated methods, choose Tools | Templates.
    }

}
