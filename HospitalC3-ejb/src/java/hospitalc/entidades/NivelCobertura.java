package hospitalc.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class NivelCobertura implements Serializable {

    @Id
    private Integer id;

    @Basic
    private String descripcion;

    public NivelCobertura() {

    }

    @Override
    public int hashCode() {
        return 31 * this.getId() + 1 ;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof NivelCobertura) && (this.id.equals(((NivelCobertura) o).id)); //To change body of generated methods, choose Tools | Templates.
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return id.toString() + " " + descripcion.toString(); //To change body of generated methods, choose Tools | Templates.
    }

}
