package hospitalc.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Version;

@Entity
public class Cama implements Serializable {

    @ManyToOne(optional = false, targetEntity = Planta.class)
    private Planta planta;

    @Id
    private Long N_Cama;
    
    //modificacion para acceso concurrente
    @Version
    private int concurrente;
    
    //necesario saber a que paciente esta asignada la cama
    @OneToOne(optional = true)
    private Paciente paciente;
    
    private boolean libre;
    
    public Cama() {

    }

    @Override
    public String toString() {
        return N_Cama.toString() + " " + planta.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return (int)(31 * this.getN_Cama() + 1);
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Cama) && (this.getN_Cama().equals(((Cama) o).getN_Cama())); //To change body of generated methods, choose Tools | Templates.
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public void setLibre(Boolean b){
        libre=b;
    }
    public boolean getLibre(){
        return libre;
    }

    public Planta getPlanta() {
        return this.planta;
    }

    public int getConcurrente() {
        return concurrente;
    }

    public void setConcurrente(int concurrente) {
        this.concurrente = concurrente;
    }
    
    public void setPlanta(Planta planta) {
        this.planta = planta;
    }

    public Long getN_Cama() {
        return this.N_Cama;
    }

    public void setN_Cama(Long N_Cama) {
        this.N_Cama = N_Cama;
    }

}