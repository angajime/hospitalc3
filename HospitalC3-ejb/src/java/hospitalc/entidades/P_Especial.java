package hospitalc.entidades;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Entity;

@Entity
public class P_Especial extends Paciente implements Serializable {

    @Basic
    private String Enfermedad;

    public P_Especial() {

    }

    public String getEnfermedad() {
        return this.Enfermedad;
    }

    public void setEnfermedad(String Enfermedad) {
        this.Enfermedad = Enfermedad;
    }

}
