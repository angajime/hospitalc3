package hospitalc.entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class ATS extends PersonalSanitario implements Serializable {

    @Basic
    private String N_ATS;
    
    @ManyToOne(targetEntity = Paciente.class)
    private Collection<Paciente> pacientes;

    public ATS() {

    }

    public String getN_ATS() {
        return this.N_ATS;
    }

    public void setN_ATS(String N_ATS) {
        this.N_ATS = N_ATS;
    }

    public Collection<Paciente> getPacientes() {
        return pacientes;
    }

    public void setPacientes(Collection<Paciente> pacientes) {
        this.pacientes = pacientes;
    }
    
}
