package hospitalc.entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Usuario implements Serializable {

    @Id
    private String DNI;

    @Basic
    private String Movil;

    @OneToMany(targetEntity = Mensaje.class, mappedBy = "propietario")
    @JoinColumn(nullable = true)
    private Collection<Mensaje> mensajes;

    @Basic

    private String Telefono;

    @Basic

    private String Apellido1;

    @OneToMany(targetEntity = Evento.class, mappedBy = "usuario")
    @JoinColumn(nullable = true)
    private Collection<Evento> eventos;

    @Basic

    private String Nombre;

    @Basic

    private String Contrasenia;

    @Basic

    private String CorreoE;

    @Basic

    private String Apellido2;

    @Basic

    private String Direccion;
    @Basic

    private String ciudad;

    public Usuario() {

    }

    public String getMovil() {
        return this.Movil;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Usuario) && (this.getDNI().equals(((Usuario) o).getDNI())); //To change body of generated methods, choose Tools | Templates.
    }
    /**
     * Author: Adrian Marin
     * Obtiene los 8 numeros del string DNI
     * @return dni
     */
    private int numbersOfDni() {
        
        int dni=Integer.getInteger(this.getDNI().substring(0, this.getDNI().length()-1));
        return dni;
    }
    @Override
    public int hashCode() {
        return 37 * this.numbersOfDni() + 1; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return DNI.toString() + " " + Nombre + " " + mensajes.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    public void setMovil(String Movil) {
        this.Movil = Movil;
    }

    public String getDNI() {
        return this.DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Collection<Mensaje> getMensajes() {
        return this.mensajes;
    }

    public void setMensajes(Collection<Mensaje> mensajes) {
        this.mensajes = mensajes;
    }

    public String getTelefono() {
        return this.Telefono;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getApellido1() {
        return this.Apellido1;
    }

    public void setApellido1(String Apellido1) {
        this.Apellido1 = Apellido1;
    }

    public Collection<Evento> getEventos() {
        return this.eventos;
    }

    public void setEventos(Collection<Evento> eventos) {
        this.eventos = eventos;
    }

    public String getNombre() {
        return this.Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getContrasenia() {
        return this.Contrasenia;
    }

    public void setContrasenia(String Contrasenia) {
        this.Contrasenia = Contrasenia;
    }

    public String getCorreoE() {
        return this.CorreoE;
    }

    public void setCorreoE(String CorreoE) {
        this.CorreoE = CorreoE;
    }

    public String getApellido2() {
        return this.Apellido2;
    }

    public void setApellido2(String Apellido2) {
        this.Apellido2 = Apellido2;
    }

    public String getDireccion() {
        return this.Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

}
