package hospitalc.entidades;

import java.io.Serializable;

import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Column;
import javax.persistence.JoinColumn;

@Entity
public class Medico extends PersonalSanitario implements Serializable {

    @Basic
    private String N_Colegiado;

    @Basic
    private String RangoProfesional;

    @JoinColumn(nullable = true)
    @OneToMany(targetEntity = Paciente.class, mappedBy = "medicoCabecera")
    private Collection<Paciente> pacientes;

    @OneToMany(targetEntity = ActividadCalificada.class)
    private Collection<ActividadCalificada> actividadesCalificadas;
    
    

    public Medico() {

    }

    public String getN_Colegiado() {
        return this.N_Colegiado;
    }

    public void setN_Colegiado(String N_Colegiado) {
        this.N_Colegiado = N_Colegiado;
    }

    public String getRangoProfesional() {
        return this.RangoProfesional;
    }

    public void setRangoProfesional(String RangoProfesional) {
        this.RangoProfesional = RangoProfesional;
    }

    public Collection<Paciente> getPacientes() {
        return this.pacientes;
    }

    public void setPacientes(Collection<Paciente> pacientes) {
        this.pacientes = pacientes;
    }

    public Collection<ActividadCalificada> getActividadesCalificadas() {
        return this.actividadesCalificadas;
    }

    public void setActividadesCalificadas(Collection<ActividadCalificada> actividadesCalificadas) {
        this.actividadesCalificadas = actividadesCalificadas;
    }

}
