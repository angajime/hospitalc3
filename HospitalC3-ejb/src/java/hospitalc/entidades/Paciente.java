package hospitalc.entidades;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Paciente extends Usuario implements Serializable {

    @OneToOne(optional = true)
    private Cama cama;
    
    @ManyToOne(optional = false, targetEntity = Hospital.class)
    private Hospital hospital1;

    @Basic
    private Long N_SS;

    @Basic
    private String ID_Paciente;

    @OneToOne(optional = false, targetEntity = HistorialClinico.class)
    private HistorialClinico historialClinico1;

    @ManyToOne(optional = false, targetEntity = Medico.class)
    private Medico medicoCabecera;
    
    @JoinColumn(nullable = true)
    @OneToMany(targetEntity = ATS.class, mappedBy = "pacientes")
    private Collection<ATS> ats;

    @ManyToOne(optional = false, targetEntity = NivelCobertura.class)
    private NivelCobertura nivelCobertura1;

    public Paciente() {

    }
    //Añadida a ultimas la relacion deberiamos cambiar el modelo
    public void setCama(Cama cama) {
        this.cama = cama;
    }

    public Cama getCama() {
        return cama;
    }

    public Collection<ATS> getAts() {
        return ats;
    }

    public void setAts(Collection<ATS> ats) {
        this.ats = ats;
    }
    
    public Hospital getHospital1() {
        return this.hospital1;
    }

    public void setHospital1(Hospital hospital1) {
        this.hospital1 = hospital1;
    }

    public Long getN_SS() {
        return this.N_SS;
    }

    public void setN_SS(Long N_SS) {
        this.N_SS = N_SS;
    }

    public String getID_Paciente() {
        return this.ID_Paciente;
    }

    public void setID_Paciente(String ID_Paciente) {
        this.ID_Paciente = ID_Paciente;
    }

    public HistorialClinico getHistorialClinico1() {
        return this.historialClinico1;
    }

    public void setHistorialClinico1(HistorialClinico historialClinico1) {
        this.historialClinico1 = historialClinico1;
    }

    public Medico getMedicoCabecera() {
        return this.medicoCabecera;
    }

    public void setMedicoCabecera(Medico medicoCabecera) {
        this.medicoCabecera = medicoCabecera;
    }

    public NivelCobertura getNivelCobertura1() {
        return this.nivelCobertura1;
    }

    public void setNivelCobertura1(NivelCobertura nivelCobertura1) {
        this.nivelCobertura1 = nivelCobertura1;
    }

}
