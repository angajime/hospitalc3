package hospitalc.entidades;

import java.io.Serializable;

import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Hospital implements Serializable {

    @Id
    private Long idHospital;

    @OneToMany(targetEntity = Paciente.class, mappedBy = "hospital1")
    private Collection<Paciente> pacientes;

    @OneToMany(targetEntity = Planta.class, mappedBy = "hospital")
    private Collection<Planta> plantas;

    @OneToMany(targetEntity = PersonalSanitario.class, mappedBy = "hospital")
    private Collection<PersonalSanitario> trabajadores;

    @Basic
    private String Nombre;

    @Basic
    private String Direccion;

    public Hospital() {

    }

    public Long getIdHospital() {
        return this.idHospital;
    }

    @Override
    public int hashCode() {
        return (int) (31 * this.getIdHospital() +1);
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Hospital) && (this.idHospital.equals(((Hospital) o).idHospital)); //To change body of generated methods, choose Tools | Templates.
    }

    public void setIdHospital(Long idHospital) {
        this.idHospital = idHospital;
    }

    public Collection<Paciente> getPaciente1() {
        return this.pacientes;
    }

    public void setPaciente1(Collection<Paciente> paciente1) {
        this.pacientes = paciente1;
    }

    public Collection<Planta> getPlantas() {
        return this.plantas;
    }

    public void setPlantas(Collection<Planta> plantas) {
        this.plantas = plantas;
    }

    public String getNombre() {
        return this.Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDireccion() {
        return this.Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public Collection<Paciente> getPacientes() {
        return pacientes;
    }

    public void setPacientes(Collection<Paciente> pacientes) {
        this.pacientes = pacientes;
    }

    public Collection<PersonalSanitario> getTrabajadores() {
        return trabajadores;
    }

    public void setTrabajadores(Collection<PersonalSanitario> trabajadores) {
        this.trabajadores = trabajadores;
    }

    @Override
    public String toString() {
        return idHospital.toString() + " " + Nombre.toString(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
