package hospitalc.entidades;

import hospitalc.clasesapoyo.ContradiccionID;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Contradiccion implements Serializable {

    @Id
    private Long idContradiccion;
    @Basic
    private String descripcion;
    
    @ManyToOne(targetEntity = HistorialClinico.class)
    private HistorialClinico historial;

    public Contradiccion() {

    }
    
    @Override
    public boolean equals(Object o) {
        return (o instanceof Contradiccion) && (this.getIdContradiccion().equals(((Contradiccion) o).getIdContradiccion())); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return (int) (31 * this.getIdContradiccion() + 1);
    }

    public HistorialClinico getHistorial() {
        return historial;
    }

    public void setHistorial(HistorialClinico historial) {
        this.historial = historial;
    }
    

    @Override
    public String toString() {
        return idContradiccion.toString() + " " + descripcion.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    public void setIdContradiccion(Long idContradiccion) {
        this.idContradiccion = idContradiccion;
    }

    public Long getIdContradiccion() {
        return idContradiccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
