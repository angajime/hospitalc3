package hospitalc.entidades;

import java.io.Serializable;
import java.sql.Timestamp;

import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Mensaje implements Serializable {

    @Id
    private Long idMensaje;

    @ManyToOne(targetEntity = Usuario.class)
    private Usuario propietario;

    @Basic
    private String campo;

    @Basic
    private Timestamp fechayhora;

    public Mensaje() {

    }

    @Override
    public int hashCode() {
       return (int) (31 * this.getIdMensaje() +1);
    }

    public Long getIdMensaje() {
        return this.idMensaje;
    }

    public Timestamp getFechayhora() {
        return fechayhora;
    }

    public void setFechayhora(Timestamp fechayhora) {
        this.fechayhora = fechayhora;
    }

    public void setIdMensaje(Long idMensaje) {
        this.idMensaje = idMensaje;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Mensaje) && (this.idMensaje.equals(((Mensaje) o).idMensaje)); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return idMensaje.toString() + " " + propietario.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    public Usuario getPropietario() {
        return propietario;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public void setPropietario(Usuario propietario) {
        this.propietario = propietario;
    }

}
