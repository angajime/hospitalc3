package hospitalc.entidades;

import hospitalc.clasesapoyo.ActividadCalificadaID;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ActividadCalificada implements Serializable {

    @Id
    private Long id;

    @Basic
    private Timestamp fechayhora;

    @ManyToOne(targetEntity = Medico.class,optional = true)
    private Medico medico;
    public ActividadCalificada() {

    }
    
    @Basic
    private String descripcion;

    @Override
    public int hashCode() {
        return (int)(31 * this.getId() + 1); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof ActividadCalificada) && (this.getId().equals(((ActividadCalificada) o).getId())); //To change body of generated methods, choose Tools | Templates.
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    
    public Timestamp getFechayhora() {
        return fechayhora;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setFechayhora(Timestamp fechayhora) {
        this.fechayhora = fechayhora;
    }

    @Override
    public String toString() {
        return id.toString() + " " + fechayhora.toString(); //To change body of generated methods, choose Tools | Templates.
    }

}
