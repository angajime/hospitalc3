package hospitalc.entidades;

import hospitalc.clasesapoyo.EpisodioID;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Episodio implements Serializable {

    @Id
    private Long idEpisodio;
    @Basic
    private String descripcion;
    @Basic
    private String patologia;
    
    @ManyToOne(targetEntity = HistorialClinico.class)
    private HistorialClinico historial;

    public Episodio() {

    }

    public void setHistorial(HistorialClinico historial) {
        this.historial = historial;
    }

    public HistorialClinico getHistorial() {
        return historial;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Episodio) && (this.idEpisodio.equals(((Episodio) o).idEpisodio)); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return idEpisodio.toString() + " " + descripcion.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getPatologia() {
        return patologia;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setPatologia(String patologia) {
        this.patologia = patologia;
    }

    @Override
    public int hashCode() {
        return (int) (31 * this.getIdEpisodio() + 1); //To change body of generated methods, choose Tools | Templates.
    }

    public void setIdEpisodio(Long idEpisodio) {
        this.idEpisodio = idEpisodio;
    }

    public Long getIdEpisodio() {
        return idEpisodio;
    }

}
