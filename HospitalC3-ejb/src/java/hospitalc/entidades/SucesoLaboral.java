package hospitalc.entidades;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
//Refactor de sucesoslaborales a SucesoLaboral -> plural a singular

@Entity
public class SucesoLaboral implements Serializable {

    @Id
    private Long idSuceso;
    
    @Basic
    private String descripcion;
    
    @Basic
    private Timestamp fechayhora;
    
    public SucesoLaboral() {

    }
    @ManyToOne(targetEntity = PersonalSanitario.class, optional = true)
    private PersonalSanitario personal;
    
    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

    public Long getIdSuceso() {
        return idSuceso;
    }

    public void setIdSuceso(Long idSuceso) {
        this.idSuceso = idSuceso;
    }
    
 
    public String getDescripcion() {
        return descripcion;
    }

    public Timestamp getFechayhora() {
        return fechayhora;
    }

   
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setFechayhora(Timestamp fechayhora) {
        this.fechayhora = fechayhora;
    }

 
    @Override
    public boolean equals(Object o) {
        return (o instanceof SucesoLaboral) && (this.getIdSuceso().equals(((SucesoLaboral) o).getIdSuceso())); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return getIdSuceso().toString(); //To change body of generated methods, choose Tools | Templates.
    }

}
