/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospitalc.backing;

import hospitalc.ejb.GestionUsuario;
import hospitalc.entidades.ATS;
import hospitalc.entidades.Medico;
import hospitalc.entidades.P_Especial;
import hospitalc.entidades.Paciente;
import hospitalc.entidades.PersonalSanitario;
import hospitalc.entidades.Usuario;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author tarazaky
 */
@SessionScoped
@Named("controlSession")
public class ControlSession implements Serializable{
    
    @Inject
    private GestionUsuario gestion;
    
    private Usuario usuario;
    private Boolean auth;
    
    private String pass1;
    private String pass2;
    
    public void update(){
        gestion.registarUsuario(usuario);
    }
    
    public void updatePass(){
        if(pass1.equals(pass2))
            gestion.registarUsuario(usuario);
    }
    
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setAuth(Boolean auth) {
        this.auth = auth;
    }

    public Boolean isAuth() {
        return auth;
    }
    
    public String getName(){
        return usuario.getNombre();
    }
    public String getApellido1() {
        return usuario.getApellido1();
    }
    public String getApellido2() {
        return usuario.getApellido2();
    }
    public String getDNI() {
        return usuario.getDNI();
    }
    public String getMovil() {
        return usuario.getMovil();
    }
    
    public void setMovil(String movil){
        usuario.setMovil(movil);
    }
    
    public String getFijo() {
        return usuario.getTelefono();
    }
    
    public void setFijo(String fijo){
        usuario.setTelefono(fijo);
    }
    
    public String getCorreoE() {
        return usuario.getCorreoE();
    }
    public String getCiudad() {
        return usuario.getCiudad();
    }
    
    public void setCiudad(String ciudad){
        usuario.setCiudad(ciudad);
    }
    
    public String getDir() {
        return usuario.getDireccion();
    }
    
    public void setDir(String dir){
        usuario.setDireccion(dir);
    }

    public void setPass1(String pass1) {
        this.pass1 = pass1;
    }

    public void setPass2(String pass2) {
        this.pass2 = pass2;
    }

    public String getPass1() {
        return pass1;
    }

    public String getPass2() {
        return pass2;
    }
    
    
    public String home() {
        // Devuelve la página Home dependiendo del rol del usuario
        if (usuario == null)
        {
            return "login.xhtml";
        }
        // else
       /* switch (usuario.getRol()) {
            case ADMINISTRADOR:
                return "admin.xhtml";
            case NORMAL:
                return "normal.xhtml";
        }*/
        
        
        if(usuario instanceof Paciente){
            return "paciente.xhtml";
        }else if(usuario instanceof ATS){
            return "ats.xhtml";
        }else if(usuario instanceof Medico){
            return "medico.xhtml";
        }else if(usuario instanceof PersonalSanitario){
            return "personal.xhtml";
        }else if(usuario instanceof P_Especial){
            return null;
        }
        return "login.xhtml";
    }
    /**
     * Author: Adrian Marin
     * 
     * @return String of type of user it is.
     */
    public String typeOfUser () {
       if (usuario == null)
        {
            return "null";
        }
        // else
       /* switch (usuario.getRol()) {
            case ADMINISTRADOR:
                return "admin.xhtml";
            case NORMAL:
                return "normal.xhtml";
        }*/
        
        
        if(usuario instanceof Paciente){
            return "paciente";
        }else if(usuario instanceof ATS){
            return "ats";
        }else if(usuario instanceof Medico){
            return "medico";
        }else if(usuario instanceof PersonalSanitario){
            return "personal";
        }else if(usuario instanceof P_Especial){
            return "null";
        }
        return "null";
    }
    
    public String logout()
    {
        // Destruye la sesión (y con ello, el ámbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        usuario = null;
        return "login.xhtml";
    }
   

    /**
     * Creates a new instance of ControlAutorizacion
     */
    public ControlSession() {
        System.out.println("SESION CREADA");
        auth = false;
    }
}
