/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospitalc.backing;

import hospitalc.ejb.GestionUsuario;
import hospitalc.entidades.Paciente;
import hospitalc.entidades.PersonalSanitario;
import hospitalc.entidades.Usuario;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author tarazaky
 */
@Named(value="controlPacientes")
@RequestScoped
public class ControlPacientes {
    
    @Inject
    private GestionUsuario gestorUsuarios;
    
    @Inject
    private ControlSession sesion;
    
    private Paciente p;
    private String nombre;
    private String apellido1;
    private String DNI;
    
    public ControlPacientes(){
        p = new Paciente();
    }
    
    public List<Paciente> getListaPacientes(){
        String dni = FacesContext.getCurrentInstance().
		getExternalContext().getRequestParameterMap().get("usuario");
        
        Usuario usr = gestorUsuarios.getUsuarioByDNI(dni);
        
        return gestorUsuarios.listaPacientes((PersonalSanitario)usr);
    }
    
    public String addPaciente(){
        p.setNombre(nombre);
        p.setApellido1(apellido1);
        p.setDNI(DNI);
        gestorUsuarios.addPaciente(p, (PersonalSanitario)sesion.getUsuario());
        return "listapacientes.xhtml";
    }

    public String getApellido1() {
        return apellido1;
    }

    public String getDNI() {
        return DNI;
    }

    public GestionUsuario getGestorUsuarios() {
        return gestorUsuarios;
    }

    public String getNombre() {
        return nombre;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
