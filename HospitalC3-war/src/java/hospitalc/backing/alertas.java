/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospitalc.backing;


import hospitalc.entidades.Evento;
import hospitalc.entidades.Paciente;
import hospitalc.entidades.Usuario;
import java.sql.Timestamp;
import java.util.ArrayList;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author lordSaeron
 */
@Named
@RequestScoped
public class alertas {
    
    private Long id;
    private String comentario;
    private Timestamp fecha;
    
    @Inject
    private ControlSession ctrl;
    private final ArrayList<Evento> eventos;
    
    
    //Modificaciones sin jpa
    Usuario user = new Paciente();
    /**
     * Creates a new instance of alarmas
     */
    public alertas() {
        //Ejemplos
        
        user.setDNI("12345678Z");
        user.setContrasenia("1234");
        
        
        eventos = new ArrayList<>();
        Evento aux = new Evento();
        aux.setId(Long.MIN_VALUE);
        aux.setUsuario(user);
        aux.setFechayhora(fecha);
        aux.setDescripcion("Revision Marcapasos");
        eventos.add(aux);
        
        aux.setId(Long.MIN_VALUE);
        aux.setUsuario(user);
        aux.setFechayhora(fecha);
        aux.setDescripcion("Revisón dental");
        eventos.add(aux);
        
    }
    
    public void setFecha(Timestamp f){
        fecha=f;
    }
    public void setComentario(String c){
        comentario=c;
    }
    public void setID(Long i){
        id=i;
    }
    
    public String getFecha(){
        return fecha.toString();
    }
    public String getComentario(){
        return comentario;
    }
    public String getId(){
        return id.toString();
    }
    
    public void listarEventos(Usuario u){
        for(Evento e: eventos){
            if(e.getUsuario()==user){
                //mostrar este evento
            }
        }
    }
    
    
    
}
