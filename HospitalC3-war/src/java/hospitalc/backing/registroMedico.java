/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitalc.backing;

import hospitalc.ejb.GestionUsuario;
import hospitalc.entidades.ActividadCalificada;
import hospitalc.entidades.Evento;
import hospitalc.entidades.Hospital;
import hospitalc.entidades.Medico;
import hospitalc.entidades.Mensaje;
import hospitalc.entidades.Paciente;
import hospitalc.entidades.SucesoLaboral;
import java.util.Collection;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Adrian Marin
 */
@RequestScoped
@Named(value = "registroMedico")
public class registroMedico {

    @Inject
    private GestionUsuario gestion;

    private Medico m;

    private String DNI;

    private String Movil;

    private Collection<Mensaje> mensajes;
    private String Telefono;
    private String Apellido1;
    private Collection<Evento> eventos;
    private String Nombre;
    private String Contrasenia;
    private String CorreoE;
    private String Apellido2;
    private String Direccion;
    private String ciudad;
    private Collection<SucesoLaboral> sucesosLaborales1;
    private Hospital hospital;
    private String RangoProfesional;
    private Collection<Paciente> pacientes;
    private Collection<ActividadCalificada> actividadesCalificadas;
    private String ID_Trabajador;

    public registroMedico() {
        this.m = new Medico();
    }
    public void updateMedico() {
        m.setApellido1(Apellido1);
        m.setApellido2(Apellido2);
        m.setCiudad(ciudad);
        m.setContrasenia(Contrasenia);
        m.setCorreoE(CorreoE);
        m.setDNI(DNI);
        m.setDireccion(Direccion);
        //ESTO VA A PETAR ASI A PELO
        m.setHospital(hospital);
        m.setID_Trabajador(ID_Trabajador);
        m.setMovil(Movil);
        m.setN_Colegiado(Telefono);
        m.setNombre(Nombre);
        m.setRangoProfesional(RangoProfesional);
        m.setTelefono(Telefono);
        this.registrar();
    }
    public void registrar() {
        gestion.registarUsuario(m);
    }

    public void setActividadesCalificadas(Collection<ActividadCalificada> actividadesCalificadas) {
        this.actividadesCalificadas = actividadesCalificadas;
    }

    public void setApellido1(String Apellido1) {
        this.Apellido1 = Apellido1;
    }

    public void setApellido2(String Apellido2) {
        this.Apellido2 = Apellido2;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public void setContrasenia(String Contrasenia) {
        this.Contrasenia = Contrasenia;
    }

    public void setCorreoE(String CorreoE) {
        this.CorreoE = CorreoE;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public void setEventos(Collection<Evento> eventos) {
        this.eventos = eventos;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public void setID_Trabajador(String ID_Trabajador) {
        this.ID_Trabajador = ID_Trabajador;
    }

    public void setM(Medico m) {
        this.m = m;
    }

    public void setMensajes(Collection<Mensaje> mensajes) {
        this.mensajes = mensajes;
    }

    public void setMovil(String Movil) {
        this.Movil = Movil;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public void setPacientes(Collection<Paciente> pacientes) {
        this.pacientes = pacientes;
    }

    public void setRangoProfesional(String RangoProfesional) {
        this.RangoProfesional = RangoProfesional;
    }

    public void setSucesosLaborales1(Collection<SucesoLaboral> sucesosLaborales1) {
        this.sucesosLaborales1 = sucesosLaborales1;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public Collection<ActividadCalificada> getActividadesCalificadas() {
        return actividadesCalificadas;
    }

    public String getApellido1() {
        return Apellido1;
    }

    public String getApellido2() {
        return Apellido2;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getContrasenia() {
        return Contrasenia;
    }

    public String getCorreoE() {
        return CorreoE;
    }

    public String getDNI() {
        return DNI;
    }

    public String getDireccion() {
        return Direccion;
    }

    public Collection<Evento> getEventos() {
        return eventos;
    }

    public GestionUsuario getGestion() {
        return gestion;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public String getID_Trabajador() {
        return ID_Trabajador;
    }

    public Medico getM() {
        return m;
    }

    public Collection<Mensaje> getMensajes() {
        return mensajes;
    }

    public String getMovil() {
        return Movil;
    }

    public String getNombre() {
        return Nombre;
    }

    public Collection<Paciente> getPacientes() {
        return pacientes;
    }

    public String getRangoProfesional() {
        return RangoProfesional;
    }

    public Collection<SucesoLaboral> getSucesosLaborales1() {
        return sucesosLaborales1;
    }

    public String getTelefono() {
        return Telefono;
    }
    
}
