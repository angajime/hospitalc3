/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospitalc.backing;

import hospitalc.ejb.GestionUsuario;
import hospitalc.entidades.Evento;
import hospitalc.entidades.Usuario;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Adrian Marin
 */
@Named(value="controlEventos")
@RequestScoped
public class ControlEventos {
    @Inject
    private GestionUsuario gestion;
    
    private Evento evento;
    private Usuario usuario;
    
    public ControlEventos() {
        evento = new Evento();
        usuario = new Usuario();
    }
    
    public List<Evento> getEventos() {
        return gestion.listaEventos(usuario);
    }
    
    
}
