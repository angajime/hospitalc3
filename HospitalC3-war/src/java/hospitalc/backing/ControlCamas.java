/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospitalc.backing;

import hospitalc.ejb.Camas;
import hospitalc.entidades.Cama;
import hospitalc.entidades.Planta;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 *
 * @author Antonio Gutiérrez Ojeda
 */
@RequestScoped
public class ControlCamas {
    
    //el ejb necesario
    @Inject
    private Camas camas;
    //clase con la que se trabaja
    private Cama cama;
    
    public ControlCamas(){
        cama = new Cama();
    }
    
    public Cama getCama(){
        return cama;
    }
    public void setCama(Cama c){
        cama=c;
    }
    
    public String asignarCama(Cama c){
        Camas.Error e = camas.asignarCama(c);
        switch(e){
            case  NO_ERROR:
            case CAMA_OCUPADA:
                   return "CamaOcupada.xhtml"; //si la cama está ocupada debe avisarse
            case CAMA_INEXISTENTE:
                    return "NoExisteCama.xhtml";
        }
        return null;
    }
    
    public String liberarCama(Cama c){
        Camas.Error e = camas.liberarCama(c);
        switch(e){
            case  NO_ERROR:
            case CAMA_INEXISTENTE:
                    return "NoExisteCama.xhtml";
        }
        return null;
    }
    
    //tengo que te ner una clase planta con un get y un set?
    public List<Cama> listarDisponibles(Planta p){
        return camas.verDisponibles(p);
    }
    
    
}
