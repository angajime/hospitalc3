/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitalc.backing;

import hospitalc.ejb.GestionUsuario;
import hospitalc.entidades.ATS;
import hospitalc.entidades.Medico;
import hospitalc.entidades.Mensaje;
import hospitalc.entidades.Paciente;
import hospitalc.entidades.PersonalSanitario;
import hospitalc.entidades.Usuario;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import hospitalc.ejb.GestionUsuario.Error;

/**
 *
 * @author tarazaky
 */
@Named(value = "login")
@RequestScoped
public class Login {

    private String DNI;
    private String contrasenia;
    private List<Usuario> usuarios;
    private Collection<Mensaje> listaMensajes;

    @Inject
    private GestionUsuario gestorUsuario;

    @Inject
    private ControlSession ctrl;

    /**
     * Creates a new instance of Login
     */
    public Login() {
        /*
        usuarios = new ArrayList<>();
        Usuario aux = new Paciente();
        //Añadiendo paciente

        aux.setDNI("12345678Z");
        aux.setNombre("Arthur");
        aux.setApellido1("Dent");
        aux.setContrasenia("1234");
        aux.setCorreoE("arthurd@gmail.com");
        aux.setMovil("616854120");
        aux.setCiudad("Málaga");
        aux.setMensajes(null);
        usuarios.add(aux);

        //Añadiendo ATS
        aux = new ATS();
        aux.setDNI("23456789D");
        aux.setNombre("Martha");
        aux.setApellido1("Jones");
        aux.setContrasenia("1234");
        aux.setCorreoE("marthaj@gmail.com");
        aux.setMovil("684230418");
        aux.setCiudad("Málaga");
        aux.setMensajes(null);
        usuarios.add(aux);

        //Añadiento medico
        aux = new Medico();
        aux.setDNI("34567890V");
        aux.setNombre("Tom");
        aux.setApellido1("Baker");
        aux.setContrasenia("1234");
        aux.setMensajes(null);
        aux.setMovil("647825950");
        aux.setCiudad("Málaga");
        aux.setCorreoE("tomb@gmail.com");
        ((Medico) aux).setN_Colegiado("1");
        usuarios.add(aux);
        */
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public List<Usuario> getMedico() {
        List<Usuario> medicos = new ArrayList<>();
        for (Usuario user : this.usuarios) {
            if (user instanceof PersonalSanitario) {
                medicos.add(user);
            }
        }
        return medicos;
    }

    public List<Usuario> getATS() {
        /*
        List<Usuario> ats = new ArrayList<>();
        for (Usuario user : this.usuarios) {
            if (user instanceof ATS) {
                ats.add(user);
            }
        }
        */
        return null;
    }

    public List<Usuario> getDoctores() {
        /*
        List<Usuario> doctores = new ArrayList<>();
        for (Usuario user : this.usuarios) {
            if (user instanceof Medico) {
                doctores.add(user);
            }
        }*/
        return null;
    }

    public String getDNI() {
        return DNI;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setUsuario(String usuario) {
        this.DNI = usuario;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String autenticar() {
        Usuario usr = new Usuario();
        usr.setDNI(this.DNI);
        usr.setContrasenia(this.contrasenia);
        Error e = gestorUsuario.compruebaLogin(usr);

        if (e.equals(Error.NO_ERROR)) {
            usr = gestorUsuario.getUsuarioByDNI(usr.getDNI());
            ctrl.setUsuario(usr);
            ctrl.setAuth(true);
            return ctrl.home();
        } else if (e.equals(Error.CONTRASENIA_INCORRECTA)) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Contraseña no válida", "Contraseña no válida"));
            return null;
        } else {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El usuario no se encuentra", "El usuario no se encuentra"));
            return null;
        }
        /*
        for (Usuario u : usuarios) {
            if (u.getDNI().equals(DNI)) {
                if (u.getContrasenia().equals(contrasenia)) {
                    // Va a una u otra dependiendo del rol
                    ctrl.setUsuario(u);
                    ctrl.setAuth(true);
                    return ctrl.home();
                } else {
                    // Falló la contraseña
                    FacesContext ctx = FacesContext.getCurrentInstance();
                    ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Contraseña no válida", "Contraseña no válida"));
                    return null;
                }
            }
        }
        // El usuario no se encuentra
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El usuario no se encuentra", "El usuario no se encuentra"));
        return null;
        */
    }
}
