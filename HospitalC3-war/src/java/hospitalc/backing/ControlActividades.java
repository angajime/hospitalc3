/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospitalc.backing;

import hospitalc.ejb.GestionUsuario;
import hospitalc.entidades.ActividadCalificada;
import hospitalc.entidades.Medico;
import hospitalc.entidades.Usuario;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Adrian Marin
 */
@Named(value="controlActividades")
@RequestScoped
public class ControlActividades {
    @Inject
    private GestionUsuario gestion;
    
    private ActividadCalificada actividad;
    private Medico m;
    
    public ControlActividades() {
        this.actividad= new ActividadCalificada();
        this.m = new Medico();
    }
    
    public List<ActividadCalificada> getlistaActividades() {
        return gestion.listaActividades(m);
    }
}
