/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospitalc.backing;


import hospitalc.ejb.GestionAlmacen;
import hospitalc.entidades.Almacen;
import hospitalc.entidades.MaterialSanitario;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author tarazaky
 */
@Named(value="controlinventario")
@RequestScoped
public class ControlInventario {
    //injectamos el EJB
    @Inject
    private GestionAlmacen gestorAlmacen;
    
    //Clase gestorAlmacen entidad
    private Almacen inventario;
    
    private MaterialSanitario ms;
    private String nombre;
    private String tipo;
    private Integer cantidad;
    private Integer codigo;
    
    public ControlInventario(){
        inventario = new Almacen();
        inventario.setIdAlmacen(1l);
        ms = new MaterialSanitario();
    }
    
    public List<MaterialSanitario> getListaMaterialSanitario(){
        //return inventario.getMateriales();
        
        return gestorAlmacen.getInventario(inventario);
    }
    
    public String addMaterialSanitario(){
        ms.setCantidad(cantidad);
        ms.setId(codigo);
        ms.setNombre(nombre);
        ms.setTipo(tipo);
        ms.setAlmacen(inventario);
        gestorAlmacen.agnadirMaterial(ms);
        return "inventario.xhtml#"; //test
    }
    
    public String rmMaterialSanitario(){
        ms.setCantidad(cantidad);
        ms.setId(codigo);
        ms.setNombre(nombre);
        ms.setTipo(tipo);
        ms.setAlmacen(inventario);
        gestorAlmacen.quitarMaterial(ms);
        return "inventario.xhtml#"; //test
    }
    
    public void setMaterialSanitario(MaterialSanitario m){
        ms = m;
    }
    public MaterialSanitario getMaterialSanitario(){
        return ms;
    }
    
    public void setTipo(String t){
        this.tipo = t;
    }

    public GestionAlmacen getGestorAlmacen() {
        return gestorAlmacen;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public Almacen getInventario() {
        return inventario;
    }

    public MaterialSanitario getMs() {
        return ms;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setGestorAlmacen(GestionAlmacen gestorAlmacen) {
        this.gestorAlmacen = gestorAlmacen;
    }

    public void setInventario(Almacen inventario) {
        this.inventario = inventario;
    }

    public void setMs(MaterialSanitario ms) {
        this.ms = ms;
    }
    
    
    public void setNombre(String n){
        this.nombre = n;
    }
    
    public void setCantidad(Integer i){
        this.cantidad = i;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getCodigo() {
        return codigo;
    }
    
    
    
}
