/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospitalc.backing;

import hospitalc.ejb.GestionUsuario;
import hospitalc.entidades.Mensaje;
import hospitalc.entidades.Usuario;
import java.sql.Timestamp;
import java.util.Collection;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Adolfo
 */

@Named(value="controlMensajes")
@RequestScoped
public class ControlMensajes {
    
    @Inject
    private GestionUsuario gMensaje;
    
    private Long id;
    private Usuario propietario;
    private String texto;
    private Timestamp fechaYHora;
    
    public ControlMensajes (){
        propietario= new Usuario();
    }
    
    public Collection<Mensaje> getMensajes(){
        return gMensaje.listaMensajes(propietario);
    }
    
}
